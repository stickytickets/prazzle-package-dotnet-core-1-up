var spawn = require('child_process').spawn;
var async = require('async');
var tmp = require('tmp');
var fs = require('fs');
var path = require('path');
var rimraf = require('rimraf');

function createResult(options) {
    return [{
        file: path.resolve(options.packageFilepath),
        metadata: {
            name: options.project,
            version: options.version,
            extension: path.extname(options.packageFilepath),
            packageType: 'dotnet-core-1-up',
            deployment: 'server',
            install: options.install,
            env: options.environmentKeys
        }
    }];
}

module.exports = function (options, cb) {
	options.log.info('Packing .Net Core App To: %s', options.outputDir);

	async.waterfall([
		async.apply(runDnuPublish, options),
		getRequiredEnvironmentKeys,
		zipDirectory,
		writeEnvironmentKeys
	], function (err) {

		if (err) return cb(err);

		cb(null, createResult(options));
	});
}

function cleanDir(options, cb) {
	options.fn.cleanDir(options.outputDir, function (err) { cb(err, options); })
}

function runDnuPublish(options, cb) {

	var dnu_publish = spawn('dotnet', ['publish', '-o', options.tmpDir, '--configuration', 'Release']);
	var hasError;

	dnu_publish.stdout.on('data', (data) => {
		options.log.debug(data.toString());
	});

	dnu_publish.stderr.on('data', (data) => {
		hasError = true;
		options.log.error(data.toString());
	});

	dnu_publish.on('close', (code) => {

		if (hasError) return callback('An error occured during the dnu publish command.');

		options.log.info('.Net Core publish completed');

		return cb(null, options);
	});
}

// function updateProjectJsonInPublishDir(options, cb) {
// 	options.packageJson.name = options.packageJson.name || options.project;
// 	fs.writeFileSync(path.join(options.tmpDir, 'project.json'), JSON.stringify(options.packageJson));
// 	cb(null, options);
// }

function getRequiredEnvironmentKeys(options, cb) {
	options.log.verbose('Reading environment keys...');

	options.environmentKeys = options.fn.envHelper.read('.env.required');

	cb(null, options);
}

function writeEnvironmentKeys(options, cb) {

	options.log.verbose('Writing environment keys...');

	var outputFilename = options.packageEnvFilepath

	console.log(options.environmentKeys);
	options.fn.envHelper.writeKeys(outputFilename, options.environmentKeys);

	cb(null, options);
}


function zipDirectory(options, cb) {
	var outputFilename = options.packageFilepath;

	if (fs.existsSync(outputFilename)) fs.unlinkSync(outputFilename);

	options.fn.zipHelper.zipDir(options.tmpDir, outputFilename, function (e, r) { cb(e, options) });
}

//--
//return options.fn.readEnvZipAndUpload({version: version}, cb);
