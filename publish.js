var async = require('async');
var fs = require('fs');
var extend = require('util')._extend;

module.exports = function(options, cb)
{
	options.log.info('Publishing .Net Core To: %s', options.s3Bucket);
    console.warn('Optoipns', options)
	async.waterfall([
		async.apply(packgeIfNotExisting, options),
		uploadToS3
	], cb);

	//options.packageFilepath(options);
}


function packgeIfNotExisting(options,cb)
{
	if(!fs.existsSync(options.packageFilepath)) {
		return require('./package')(options,cb);
	}

	cb(null, options);
}


function uploadToS3(options,cb)
{

	var file = options.packageFilepath;
	var env  = options.fn.envHelper.read(	options.packageEnvFilepath  );

	return options.fn.s3.upload({
		keyBase: 'apps',
		file: file,
		bucket: options.s3Bucket,
		name: options.project,
		version: options.version,
		metadata: env
	}, cb);
}
